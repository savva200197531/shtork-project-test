// photo preview

let fr = new FileReader();


document.querySelector(".post-photo").addEventListener("change", function () {
  if (this.files[0]) {


    fr.addEventListener("load", function () {
      document.querySelector(".photo-preview").style.backgroundImage = "url(" + fr.result + ")";
    }, false);

    fr.readAsDataURL(this.files[0]);

  }
});

// post and commentaries


function postClick() {
  const newPost = document.querySelector('.feed-post');
  const post = document.querySelector(".post");
  const postText = document.querySelector(".post-input");

  const div = document.createElement("div");
  div.className = "divA";

  const postImg = document.createElement("img");
  postImg.className = "post-img";
  postImg.src = fr.result;

  const postPasta = document.createElement("div");
  postPasta.className = "post-pasta";
  postPasta.innerHTML = postText.value;

  const comments = document.createElement('button');
  comments.className = 'comments-btn';
  comments.innerHTML = 'comment';

  const comm = document.createElement('input');
  comm.className = 'comm';
  comm.placeholder = 'Написать комментарий...';

  const commentSection = document.createElement('div');
  commentSection.className = 'comment-section';

  const commentSectionButtons = document.createElement('div');
  commentSectionButtons.className = 'comment-section-buttons';

  const commBtn = document.createElement('button');
  commBtn.className = 'comm-btn';
  commBtn.innerHTML = '';

  const commBtn2 = document.createElement('button');
  commBtn2.className = 'comm-btn';
  commBtn2.innerHTML = '';

  const commentList = document.createElement('div');
  commentList.className = 'comment-list';




  commBtn2.addEventListener('click', function () {
    const leaveComment = document.createElement('div');
    leaveComment.className = 'leave-comment';
    leaveComment.innerHTML = comm.value;

    const userName = document.createElement('div');
    userName.className = 'user-name';
    userName.innerHTML = 'Дмитрий Питосин';

    const commentAnswer = document.createElement('div');
    commentAnswer.className = 'comment-answer';
    commentAnswer.innerHTML = 'Ответить';


    if (comm.value === '') {
      return;
    } else {

      postPasta.appendChild(commentList);
      commentList.appendChild(userName);
      commentList.appendChild(leaveComment);
      leaveComment.appendChild(commentAnswer);

    }

    commentAnswer.addEventListener('click', function () {

      const leaveAnswer2 = document.createElement('div');
      leaveAnswer2.className = 'leave-answer2';
      leaveComment.appendChild(leaveAnswer2);

      const commentAnswerInput = document.createElement('input');
      commentAnswerInput.className = 'comment-answer-input';
      commentAnswerInput.placeholder = 'Ответить';

      const commentAnswerBtn = document.createElement('button');
      commentAnswerBtn.className = 'comment-answer-btn';


      const answerList = document.createElement('div');
      answerList.className = 'answer-list';

      commentAnswerBtn.addEventListener('click', leaveAnswer);


      leaveAnswer2.appendChild(commentAnswerInput);
      leaveAnswer2.appendChild(commentAnswerBtn);


      const commentAnswerText = document.createElement('div');
      commentAnswerText.className = 'comment-answer-text';

      const userName2 = document.createElement('div');
      userName2.className = 'user-name2';
      userName2.innerHTML = 'Гена Горин';


      function leaveAnswer() {
        if (commentAnswerInput.value === '') {
          return null;
        } else {
          commentAnswerText.innerHTML = commentAnswerInput.value;
          leaveAnswer2.before(userName2);
          leaveAnswer2.before(commentAnswerText);
          commentAnswerInput.value = '';
          commentAnswerInput.remove();
          commentAnswerBtn.remove();
          leaveAnswer2.remove();
        }
      }


    });


    comm.value = '';


  });

  if (fr.result !== null) {
    div.appendChild(postImg);
  }

  if (fr.result === null && postText.value === '') {
    return console.error('empty post');
  } else {
    post.prepend(div);

    div.appendChild(postPasta);
    div.appendChild(commentSection);
    commentSection.appendChild(comm);
    commentSection.appendChild(commentSectionButtons);
    commentSectionButtons.appendChild(commBtn);
    commentSectionButtons.appendChild(commBtn2);
  }

  const photoPreview = document.querySelector('.photo-preview');


  photoPreview.style = '';
  fr = new FileReader();


  postText.value = "";
}

const postBtn = document.querySelector('.post-btn');
postBtn.addEventListener('click', postClick);


// post button



// time

let clock = document.querySelector('.clock');

function update() {

  let date = new Date();
  let hours = date.getHours();
  if (hours < 10) hours = '0' + hours;
  clock.children[0].innerHTML = hours;

  let minutes = date.getMinutes();
  if (minutes < 10) minutes = '0' + minutes;
  clock.children[1].innerHTML = minutes;
}

update();
setInterval(update, 1000);

















